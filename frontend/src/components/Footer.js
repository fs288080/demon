import { Flex, Text, Heading, Link, Icon, Divider } from "@chakra-ui/react";
import { BsYoutube, BsInstagram, BsFacebook, BsTwitter } from "react-icons/bs";
const MenuItem = ({ children, url }) => {
  return (
    <Link
      href={url}
      fontSize="sm"
      letterSpacing="wide"
      color="blue.200"
      fontWeight="bold"
      textTransform="uppercase"
      mr="5"
      display="block"
      _hover={{ color: "blue.100" }}
      mt={{ base: "4", md: "0" }}
      textDecoration="none"
      textAlign="center"
    >
      {children}
    </Link>
  );
};

const Footer = () => {
  return (
    <Flex
      as="footer"
      alignItems="center"
      justify="space-around"
      wrap="wrap"
      py="6"
      px="6"
      bgGradient="linear(to-r, blue.900, blue.600)"
      mt="1"
      flexDirection="row"
      textAlign="center"
    >
      <Flex flexDirection="column" align="center" textAlign="center">
        <MenuItem>Help Center</MenuItem>
        <MenuItem>Warranty</MenuItem>
        <MenuItem>Contact Us</MenuItem>
        <MenuItem>Product</MenuItem>
        <MenuItem>Help</MenuItem>
        <MenuItem>Order</MenuItem>
        <MenuItem>Status</MenuItem>
      </Flex>

      <Flex flexDirection="column" align="center">
        <MenuItem>Discount</MenuItem>
        <MenuItem>Programs</MenuItem>
        <MenuItem>Custom</MenuItem>
        <MenuItem>Bulk</MenuItem>
        <MenuItem>Releases</MenuItem>
      </Flex>

      <Flex flexDirection="column" align="center">
        <MenuItem>About</MenuItem>
        <MenuItem>Born In</MenuItem>
        <MenuItem>Our</MenuItem>
        <MenuItem>Careers</MenuItem>
      </Flex>

      <Flex flexDirection="column" align="center">
        <Heading
          as="h2"
          color="blue.200"
          fontWeight="bold"
          size="md"
          letterSpacing="md"
          py="3"
        >
          FOLLOW US
        </Heading>

        <Flex justify="center">
          <MenuItem>
            <Icon as={BsInstagram} />
          </MenuItem>
          <MenuItem>
            <Icon as={BsFacebook} />
          </MenuItem>
          <MenuItem>
            <Icon as={BsYoutube} />
          </MenuItem>
          <MenuItem>
            <Icon as={BsTwitter} />
          </MenuItem>
        </Flex>
      </Flex>

      <Divider orientation="horizontal" color="blue.200" py="3"></Divider>

      <Flex>
        <Text color="blue.200" pr="10">
          <Link>Privacy Policy </Link>&#124;
          <Link>Terms of Use </Link>&#124;
          <Link>Notice of Collection</Link>
        </Text>
        <Text color="blue.200" pl="10">
          Copyright 2022. DEMON Store. All Rights Reserved.
        </Text>
      </Flex>
    </Flex>
  );
};

export default Footer;
