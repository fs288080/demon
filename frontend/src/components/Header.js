import { useState } from "react";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { Link as RouterLink } from "react-router-dom";
import {
  Flex,
  Heading,
  Link,
  Box,
  Icon,
  Menu,
  MenuButton,
  MenuList,
  MenuItem,
  Button,
} from "@chakra-ui/react";
import {
  HiShoppingBag,
  HiUser,
  HiOutlineMenuAlt3,
  HiOutlineSearch,
} from "react-icons/hi";
import { GiTRexSkull } from "react-icons/gi";
import { IoChevronDown } from "react-icons/io5";
import { logout } from "../actions/userActions";
import { USER_DETAILS_RESET } from "../constants/userConstants";

const MenuItems = ({ children, url }) => {
  return (
    <Link
      as={RouterLink}
      to={url}
      fontSize="sm"
      letterSpacing="wide"
      color="blue.200"
      fontWeight="bold"
      textTransform="uppercase"
      mr="5"
      display="block"
      _hover={{ color: "blue.100" }}
      mt={{ base: "4", md: "0" }}
      textDecoration="none"
    >
      {children}
    </Link>
  );
};

const Header = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const [show, setShow] = useState(false);

  const userLogin = useSelector((state) => state.userLogin);
  const { userInfo } = userLogin;

  const userDetails = useSelector((state) => state.userDetails);
  const { user } = userDetails;

  if (user.name) {
    userInfo.name = user.name;
  }

  const logoutHandler = () => {
    dispatch(logout());
    navigate("/login");
    dispatch({ type: USER_DETAILS_RESET });
  };

  return (
    <Flex
      as="header"
      align="center"
      justify="space-between"
      wrap="wrap"
      py="4"
      px="6"
      bgGradient="linear(to-r, blue.900, blue.600)"
      w="100%"
      top="0"
      zIndex="2"
      pos="fixed"
    >
      <Flex align="center" mr="5">
        <Heading
          as="h2"
          color="blue.200"
          fontWeight="bold"
          size="lg"
          letterSpacing="md"
        >
          <Link
            as={RouterLink}
            to="/"
            _hover={{ color: "blue.100", textDecor: "none" }}
          >
            <Icon as={GiTRexSkull} /> DEMON
          </Link>
        </Heading>
      </Flex>

      <Box
        display={{ base: "block", md: "none", sm: "block" }}
        onClick={() => setShow(!show)}
      >
        <Icon as={HiOutlineMenuAlt3} color="blue.200" w="6" h="6" />
      </Box>

      <Box
        display={{ base: show ? "block" : "none", md: "flex" }}
        width={{ base: "full", md: "auto" }}
        alignItems="center"
      >
        <MenuItems url="/cart">
          <Flex alignItems="center">
            <Icon as={HiShoppingBag} w="4" h="4" mr="1" /> CART
          </Flex>
        </MenuItems>

        {userInfo ? (
          <Menu>
            <MenuButton
              as={Button}
              rightIcon={<IoChevronDown />}
              _hover={{ textDecoration: "none", opacity: "0.7" }}
            >
              {userInfo.name}
            </MenuButton>
            <MenuList url="/login">
              <MenuItem as={RouterLink} to="profile">
                Profile
              </MenuItem>
              <MenuItem onClick={logoutHandler}>Logout</MenuItem>
            </MenuList>
          </Menu>
        ) : (
          <MenuItems url="/login">
            <Flex alignItems="center">
              <Icon as={HiUser} w="4" h="4" mr="1" /> Login
            </Flex>
          </MenuItems>
        )}

        {/* Admin Menu */}

        {userInfo && userInfo.isAdmin && (
          <Menu>
            <MenuButton
              ml="5"
              color="white"
              fontSize="sm"
              fontWeight="semibold"
              as={Link}
              textTransform="uppercase"
              _hover={{ textDecor: "none", opacity: "0.7" }}
            >
              Manage <Icon as={IoChevronDown} />
            </MenuButton>
            <MenuList>
              <MenuItem as={RouterLink} to="/admin/userlist">
                All Users
              </MenuItem>
              <MenuItem as={RouterLink} to="/admin/productlist">
                All Products
              </MenuItem>
              <MenuItem as={RouterLink} to="/admin/orderlist">
                All Orders
              </MenuItem>
            </MenuList>
          </Menu>
        )}

        {/* <MenuItems url="/">
          <Flex alignItems="center">
            <Icon as={HiOutlineSearch} w="4" h="4" mr="1" /> Search
          </Flex>
        </MenuItems> */}
      </Box>
    </Flex>
  );
};

export default Header;
