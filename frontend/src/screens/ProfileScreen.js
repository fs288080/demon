import { useState, useEffect } from "react";
import { Link as RouterLink, useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import {
  Button,
  Flex,
  Heading,
  Text,
  FormControl,
  FormLabel,
  Input,
  Link,
  Spacer,
  Grid,
  Tr,
  Th,
  Td,
  Icon,
  Table,
  Thead,
  Tbody,
  TableContainer
} from "@chakra-ui/react";
import Message from "../components/Message";
import Loader from "../components/Loader";
import FormContainer from "../components/FormContainer";
import { getUserDetails, updateUserProfile } from "../actions/userActions";
import { listMyOrders } from "../actions/orderActions";
import { IoWarning } from "react-icons/io5";

const ProfileScreen = () => {
  const dispatch = useDispatch();
  let navigate = useNavigate();

  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [message, setMessage] = useState(null);

  const userDetails = useSelector((state) => state.userDetails);
  const { loading, error, user } = userDetails;

  const userLogin = useSelector((state) => state.userLogin);
  const { userInfo } = userLogin;

  const userUpdateProfile = useSelector((state) => state.userUpdateProfile);
  const { success } = userUpdateProfile;

  const orderMyList = useSelector((state) => state.orderMyList);
  const { loading: loadingOrders, error: errorOrders, orders } = orderMyList;

  useEffect(() => {
    if (!userInfo) {
      navigate("/login");
    } else {
      if (!user.name) {
        dispatch(getUserDetails("/profile"));
        dispatch(listMyOrders());
      } else {
        setName(user.name);
        setEmail(user.email);
      }
    }
  }, [user, dispatch, userInfo, navigate, success]);

  const submitHandler = (e) => {
    e.preventDefault();
    if (password !== confirmPassword) {
      setMessage("Passwords do not match");
    } else {
      // Dispatch
      dispatch(updateUserProfile({ id: user._id, name, email, password }));
    }
  };

  return (
    <Grid
      templateColumns={{ lg: "1fr 2fr", md: "1fr", base: "1fr" }}
      py="5"
      gap="10"
    >
      <Flex w="full" alignItems="center" justifyContent="center" py="5">
        <FormContainer>
          <Heading as="h1" mb="8" fontSize="3xl">
            User Profile
          </Heading>

          {error && <Message type="error">{error}</Message>}
          {message && <Message type="error">{message}</Message>}

          <form onSubmit={submitHandler}>
            <FormControl id="name">
              <FormLabel>Name</FormLabel>
              <Input
                type="text"
                placeholder="Full Name"
                value={name}
                onChange={(e) => setName(e.target.value)}
              />
            </FormControl>

            <Spacer h="3" />

            <FormControl id="email">
              <FormLabel>Email</FormLabel>
              <Input
                type="email"
                placeholder="Email address"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
            </FormControl>

            <Spacer h="3" />

            <FormControl id="password">
              <FormLabel>Password</FormLabel>
              <Input
                type="password"
                placeholder="**********"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              />
            </FormControl>

            <Spacer h="3" />

            <FormControl id="confirmPassword">
              <FormLabel>Confirm Password</FormLabel>
              <Input
                type="password"
                placeholder="**Again**"
                value={confirmPassword}
                onChange={(e) => setConfirmPassword(e.target.value)}
              />
            </FormControl>
            <Button type="submit" isLoading={loading} mt="4" colorScheme="blue">
              Update
            </Button>
          </form>
        </FormContainer>
      </Flex>

      <Flex direction="column">
        <Heading as="h2" mb="4">
          My Orders
        </Heading>

        {loadingOrders ? (
          <Loader />
        ) : errorOrders ? (
          <Message type="error">{errorOrders}</Message>
        ) : (
          <TableContainer>
            <Table variant="unstyled">
              <Thead>
                <Tr>
                  <Th>ID</Th>
                  <Th
                    display={{ md: "inline-block", sm: "none", base: "none" }}
                  >
                    DATE
                  </Th>
                  <Th
                    display={{ md: "inline-block", sm: "none", base: "none" }}
                  >
                    TOTAL
                  </Th>
                  <Th
                    display={{ md: "inline-block", sm: "none", base: "none" }}
                  >
                    PAID
                  </Th>
                  <Th
                    display={{ md: "inline-block", sm: "none", base: "none" }}
                  >
                    DELIVERED
                  </Th>
                </Tr>
              </Thead>
              <Tbody>
                {orders.map((order) => (
                  <Tr key={order._id}>
                    <Td>{order._id}</Td>
                    <Td
                      display={{ md: "inline-block", sm: "none", base: "none" }}
                    >
                      {order.createdAt.split("T")[0]}
                    </Td>
                    <Td
                      display={{ md: "inline-block", sm: "none", base: "none" }}
                    >
                      {order.totalPrice}
                    </Td>
                    <Td
                      display={{ md: "inline-block", sm: "none", base: "none" }}
                    >
                      {order.isPaid ? (
                        order.paidAt.substring(0, 10)
                      ) : (
                        <Icon as={IoWarning} color="red" />
                      )}
                    </Td>
                    <Td
                      display={{ md: "inline-block", sm: "none", base: "none" }}
                    >
                      {order.isDelivered ? (
                        order.deliveredAt.substring(0, 10)
                      ) : (
                        <Icon as={IoWarning} color="red" />
                      )}
                    </Td>
                    <Td display="inline-block">
                      <Button
                        as={RouterLink}
                        to={`/order/${order._id}`}
                        colorScheme="blue"
                        size="sm"
                      >
                        Details
                      </Button>
                    </Td>
                  </Tr>
                ))}
              </Tbody>
            </Table>
          </TableContainer>
        )}
      </Flex>
    </Grid>
  );
};

export default ProfileScreen;
