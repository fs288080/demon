import { Heading, Grid, Flex, Image } from "@chakra-ui/react";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { listProducts } from "../actions/productActions";
import Product from "../components/Product";
import Message from "../components/Message";
import Loader from "../components/Loader";

const HomeScreen = () => {
  const dispatch = useDispatch();

  const productList = useSelector((state) => state.productList);
  const { loading, error, products } = productList;

  useEffect(() => {
    dispatch(listProducts());
  }, []);

  return (
    <>
      <Flex maxW="full" mx="auto">
        <Image src="/images/banner.jpg" maxWidth="100%" />
      </Flex>

      <Flex justifyContent="center">
        <Heading as="h2" mb="8" mt="5" fontSize="3xl">
          Latest Products
        </Heading>
      </Flex>

      {loading ? (
        <Loader />
      ) : error ? (
        <Message type="error">{error}</Message>
      ) : (
        <Flex justifyContent="center">
          <Grid
            templateColumns={{
              base: "repeat(1,1fr)",
              xl: "repeat(4,1fr)",
              lg: "repeat(3,1fr)",
              md: "repeat(2,1fr)"
            }}
            gap="8"
          >
            {products.map((product) => (
              <Product key={product._id} product={product} />
            ))}
          </Grid>
        </Flex>
      )}
    </>
  );
};

export default HomeScreen;
